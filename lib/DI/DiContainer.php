<?php

namespace Framework\DI;

use Attribute;
use Framework\Attributes\Authorize;
use Framework\Attributes\Request;
use Framework\Http\Request as HttpRequest;
use Framework\Finders\ClassFinder;
use Framework\Http\Session;

class DiContainer
{
    private static ?DiContainer $instance = null;
    public static function instance(
        string $folder = "",
        string $ctrlNamespace = "App\\Controllers",
        string $serviceNamespace = "App\\Services",
        string $repositoryNamespace = "App\\Repositories"
    ): DiContainer
    {
        self::$instance = self::$instance != null ? self::$instance : new DiContainer($ctrlNamespace, $serviceNamespace, $repositoryNamespace, $folder);
        return self::$instance;
    }

    private string $folder;
    private string $ctrlNamespace;
    private array $controllers = [];

    private string $srvNamespace;
    private string $repoNamespace;
    private array $services = [];

    private function __construct(string $ctrlNamespace, string $serviceNamespace, string $repositoryNamespace, string $folder) {
        $this->folder = $folder;
        $this->srvNamespace = $serviceNamespace;
        $this->repoNamespace = $repositoryNamespace;
        if (count($this->services) == 0) {
            $this->__hydrateServices($repositoryNamespace);
            $this->__hydrateServices($serviceNamespace);
        }
        $this->ctrlNamespace = $ctrlNamespace;
        if (count($this->controllers) == 0) {
            $this->__hydrateControllers();
        }
    }

    public function controller(string $method, string $path) {
        if (!key_exists($method, $this->controllers)) {
            return null;
        }
        if (!key_exists($path, $this->controllers[$method])) {
            return null;
        }
        return $this->controllers[$method][$path];
    }
    public function service(string $service) {
        if (!key_exists($service, $this->services)) { return null; }

        return $this->services[$service];
    }

    private function __hydrateControllers() {
        $classes = ClassFinder::getClassInNamespace($this->ctrlNamespace);
        foreach ($classes as $class) {
            $routes = $this->__hydrateController($class);

            foreach ($routes as $key => $route) {
                foreach ($route["methods"] as $method) {
                    foreach ($route["paths"] as $path) {
                        $this->controllers[$method][$path] = [
                            "ctrl" => $route["ctrl"],
                            "action" => $route["action"],
                            "params" => $route["params"],
                            "access" => $route["access"]
                        ];
                    }
                }
            }
        }
    }
    private function __hydrateController($class) {
        $routes = [];
        $reflection = new \ReflectionClass($class);
        $attrControllers = $reflection->getAttributes(\Framework\Attributes\Controller::class);

        foreach ($attrControllers as $attr) {
            $attrInstance = $attr->newInstance();
            $prefix = $attrInstance->prefix;
            $rMethods = $reflection->getMethods(); //Je récupère l'ensemble des methodes de mon controller
            $rCtrlArgs = [];
            $rCtrlConstructor = $reflection->getConstructor(); //Je récupère le constructeur de mon controller
            if ($rCtrlConstructor) {
                $rCtrlConstructorParams = $rCtrlConstructor->getParameters(); //Je récupère les paramètres de mon constructeur

                foreach ($rCtrlConstructorParams as $key => $param) {
                    $rCtrlArgs[$param->getName()] = $this->service($param->getType()->getName());
                }
                $controller = $reflection->newInstanceArgs($rCtrlArgs);
            } else {
                $controller = $reflection->newInstance();
            }

            foreach ($rMethods as $method) {
                $parameters = $this->__hydrateAction($controller, $method);
                $methodName = $method->getName();
                $aMethods = $method->getAttributes();
                $routes[$methodName] = [];;
                foreach ($aMethods as $route) {
                    $attribute = $route->newInstance();
                    if ($attribute instanceof Request) {
                        $methods = $attribute->methods;
                        $paths = array_map(fn($path) => $this->folder. $prefix. $path,$attribute->paths);
                        $ctrl = $controller;
                        $action = $methodName;
                        $params = $parameters;
                        $access = $routes[$methodName]["access"] ?? true;
                        $routes[$methodName] = [
                            "methods" => $methods, "paths" => $paths,
                            "ctrl" => $ctrl, "params" => $params,
                            "action" => $action, "access" => $access
                        ];
                    }
                    else if ($attribute instanceof Authorize) {
                        $routes[$methodName] = array_merge($routes[$methodName], ["access" => $attribute->hasAccess()]);
                    }
                }
            }
        }

        return $routes;
    }
    private function __hydrateAction($ctrl, \ReflectionMethod $method) {
        $actionParameters = [];
        $methodName = $method->getName();
        $mParameters = $method->getParameters();
        foreach ($mParameters as $param) {
            $actionParameters[$param->getName()] = $this->service($param->getType());
        }
        return $actionParameters;
    }
    private function __hydrateServices($namespace) {
        if (!$this->service(Session::class)) {
            $this->services[Session::class] = new Session();
        }
        if (!$this->service(HttpRequest::class)) {
            $this->services[HttpRequest::class] = new HttpRequest($this->service(Session::class));
        }

        $classes = ClassFinder::getClassInNamespace($namespace);
        foreach ($classes as $class) {
            $service = $this->__hydrateService($class);
            $this->services[$service["name"]] = $service["instance"];
        }
    }
    private function __hydrateService(string $class) {
        $reflection = new \ReflectionClass($class);

        $serviceName = $reflection->getName();

        $sConstructor = $reflection->getConstructor();
        if (!$sConstructor) { return null; }
        $sConstructorParams = $sConstructor->getParameters();

        $sConstructorArgs = [];
        foreach ($sConstructorParams as $sConstructorParam) {
            $type = $sConstructorParam->getType()->getName();

            if (isset($this->services[$type])) {
                $sConstructorArgs[$sConstructorParam->getName()] = $this->services[$type];
            } else {
                $service = $this->__hydrateService($type);
                $sConstructorArgs[$sConstructorParam->getName()] = $service["instance"];
            }
        }
        $service = $reflection->newInstanceArgs($sConstructorArgs);
        return ["name" => $serviceName, "instance" => $service];
    }
}