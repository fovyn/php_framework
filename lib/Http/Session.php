<?php

namespace Framework\Http;

class Session
{
    public function __construct()
    {
        session_start();
    }

    public function get(string $key) {
        return $_SESSION[$key];
    }
    public function isLogged(): bool {
        return isset($_SESSION["user"]);
    }

    public function signIn(object $user) {
        foreach ($user as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }

    public function close() {
        session_destroy();
    }
}