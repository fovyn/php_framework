<?php

namespace Framework\Http;

/**
 * Class représentant l'état de la requêtes
 */
class Request
{
    private Session $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function has(string $key, array $array) {
        return key_exists($key, $array);
    }
    public function get(string $key): mixed {
        if (!$this->has($key, $_GET)) { return null; }

        return $_GET[$key];
    }
    public function request(?string $key = null): mixed {
        if (!$key) { return $_POST; }
        if (!$this->has($key, $_POST)) { return null; }

        return $_POST[$key];
    }
    public function session(): Session {
        return $this->session;
    }

    public function isLogged() {
        return $this->has("user", $_SESSION);
    }

    public function method() {
        return $_SERVER["REQUEST_METHOD"];
    }
}