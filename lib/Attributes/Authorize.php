<?php

namespace Framework\Attributes;

#[\Attribute(\Attribute::TARGET_ALL)]
class Authorize
{
    public array $roles;

    public function __construct(array $roles = [])
    {
        $this->roles = $roles;
    }

    public function hasAccess(): bool {
        if (!isset($_SESSION["roles"])) { return false; }
        return count($this->roles) == 0 && isset($_SESSION["user"]) || count(array_intersect($this->roles, $_SESSION["roles"])) != 0;
    }
}