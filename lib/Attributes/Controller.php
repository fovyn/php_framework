<?php

namespace Framework\Attributes;

#[\Attribute(\Attribute::TARGET_CLASS)]
class Controller
{
    public string $prefix = "/";

    public function __construct(string $prefix = "")
    {
        $this->prefix = $prefix;
    }


}