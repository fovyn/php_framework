<?php

namespace Framework\Attributes;

#[\Attribute(\Attribute::TARGET_METHOD)]
class Request
{
    public array $methods;
    public array $paths;

    public function __construct(array $methods, array $paths)
    {
        $this->methods = $methods;
        $this->paths = $paths;
    }
}