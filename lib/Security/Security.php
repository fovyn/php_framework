<?php

namespace Framework\Security;

class Security
{

    static function hasRole(...$needs): bool {
        $roles = $_SESSION["roles"] ?? false;

        if (!$roles) {
            return false;
        }

        return count(array_intersect($needs, $roles)) > 0;
    }
}