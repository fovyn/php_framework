<?php

namespace Framework\Orm;

use Framework\Orm\Attributes\ManyToMany;
use Framework\Orm\Attributes\ManyToOne;
use Framework\Orm\Attributes\OneToOne;
use Framework\Orm\Repositories\ManyToManyRepository;
use Framework\Orm\Repositories\ManyToOneRepository;
use Framework\Orm\Repositories\OneToOneRepository;

abstract class Entity
{
    public abstract function getId();

    private function __hydrate(array $options = []) {
        //Créer une objet qui me permet de parcourir les caractéristiques de l'objet courant
        $reflection = new \ReflectionClass($this);

        //Récupération des propriétés de mon objet
        $properties = $reflection->getProperties();
        //Récupération des propriétés hérité du parent par mon objet
        if ($reflection->getParentClass()) {
            $parentProperties = $reflection->getParentClass()->getProperties();

            //Transformation de deux tableaux en un seul
            $properties = array_merge($parentProperties, $properties);
        }
        //Pour  chaque propriété de l'objet a hydraté, regarde si on lui donne une valeur
        foreach ($properties as $property) {
            foreach ($options as $key => $value) {
                //Si la donnée existe pour une propriété
                if ($property->getName() === $key) {
                    //Rendre modifiable la propriété
                    $property->setAccessible(true);
                    //Modifier la valeur
                    $type = $property->getType()->getName();
                    switch ($type) {
                        case "string":
                        case "float":
                        case "int":
                            $property->setValue($this, $value);
                            break;
                        default:
                            //$type = DateTime
                            // => $type = \DateTime !!!"\\" car \ est le caractère d'echapement
                            $type = "\\". $type;
                            //new \DateTime($value)
                            $property->setValue($this, new $type($value));
                            break;
                    }
                }
            }
        }
    }
    public function __construct(array $options = [])
    {
        $this->__hydrate($options);
    }

    private function handleManyToOne(ManyToOne $instance) {
        $targetType = $instance->target;
        $joinColumn = $instance->joinColum;
        $id = $this->$joinColumn;
        $targetInstance = new \ReflectionClass($targetType);
        $targetRepository = str_replace("App\\Models", "App\\Repositories", $targetInstance->getName());
        $targetRepository = new \ReflectionClass($targetRepository. "Repository");
        /** @var ManyToOneRepository $repo */
        $repo = $targetRepository->newInstance();
        $res = $repo->loadManyToOne($id);
        return $res;
    }
    private function handleManyToMany(ManyToMany $instance) {
        $targetType = $instance->target;
        $joinTable = $instance->joinTable;
        $leftColumn = $instance->leftColumn;
        $rightColumn = $instance->rightColumn;

        $targetInstance = new \ReflectionClass($targetType);
        $targetRepository = str_replace("App\\Models", "App\\Repositories", $targetInstance->getName());
        $targetRepository = new \ReflectionClass($targetRepository. "Repository");
        /** @var ManyToManyRepository $repo */
        $repo = $targetRepository->newInstance();
        $res = $repo->loadManyToMany($joinTable, $leftColumn, $this->getId(), $rightColumn);

        return $res;
    }
    private function handleOneToOne(OneToOne $instance) {
        $targetType = $instance->target;
        $joinColumn = $instance->joinColumn;

        $id = $this->$joinColumn;
        $targetInstance=  new \ReflectionClass($targetType);
        $targetRepository = str_replace("App\\Models", "App\\Repositories", $targetInstance->getName());
        $targetRepository = new \ReflectionClass($targetRepository. "Repository");
        /** @var OneToOneRepository $repo */
        $repo = $targetRepository->newInstance();
        return $repo->loadOneToOne($id);
    }
    public function __call(string $name, array $arguments)
    {
        $field = lcfirst(str_replace("get", "", $name));

        $class = new \ReflectionClass($this);

        $field = $class->getProperty($field);

        if ($field && str_starts_with($name, "get")) {
            $attributes = $field->getAttributes();
            foreach ($attributes as $attribute) {
                $instance = $attribute->newInstance();

                $res = $field->getValue($this);
                if ($instance instanceof ManyToOne && !$res) {
                    $field->setValue($this, $this->handleManyToOne($instance));
                } elseif ($instance instanceof ManyToMany && !$res) {
                    $field->setValue($this, $this->handleManyToMany($instance));
                } elseif ($instance instanceof OneToOne && !$res) {
                    $field->setValue($this, $this->handleOneToOne($instance));
                }

                return $field->getValue($this);
            }
        }
    }
}