<?php

namespace Framework\Orm;

use Framework\Orm\Attributes\Lifecycle;
use Framework\Orm\Repositories\ManyToManyRepository;
use Framework\Orm\Repositories\ManyToOneRepository;
use Framework\Orm\Repositories\OneToOneRepository;

abstract class Repository implements ManyToOneRepository, ManyToManyRepository, OneToOneRepository
{
    protected \PDO $pdo;
    protected string $tableName;
    protected string $class;

    public function __construct(string $class, string $tableName)
    {
        $this->class = $class;
        $this->tableName = $tableName;

        $json = file_get_contents(__DIR__. "/../../config/database.json");
        $dbConfig = json_decode($json, true);
        $this->pdo = new \PDO($dbConfig['DB_TYPE'].":host=". $dbConfig['DB_HOST']. ";dbname=". $dbConfig['DB_NAME'], $dbConfig['DB_USER'], $dbConfig['DB_USER_PASSWORD']);
    }

    /**
     * Fonction permettant la récupération dynamique d'une table
     * @param array $where Défini par le nom de la méthode appelée et les valeurs passée en paramètre
     * @param array $select Dernier paramètre le methode appelée (doit être un tableau-
     * @return array|null[]|object[]
     */
    private function findAll(array $where = [], array $select= []) {
        $selectStr = count($select) > 0 ? implode(", ", $select) : "*";
        $queryStr = "SELECT $selectStr FROM $this->tableName";
        $params = [];
        if (count($where) > 0) {
            $queryStr .= " WHERE ";
            $last_key = array_key_last($where);
            foreach ($where as $field => $value) {
                $queryStr .= "$field = :$field";
                $params[":$field"] = $value;
                if ($last_key != $field) {
                    $queryStr .= " AND ";
                }
            }
        }
        $query = $this->pdo->prepare($queryStr);
        $query->setFetchMode(\PDO::FETCH_ASSOC);
        $query->execute($params);

        $class = new \ReflectionClass($this->class);
        return array_map(fn($item) => $class->newInstanceArgs([$item]), $query->fetchAll());
    }

    /**
     * Fonction permettant la récupération dynamique d'une valeur de la table
     * @param array $where Défini par le nom de la méthode appelée et les valeurs passée en paramètre
     * @param array $select Dernier paramètre le methode appelée (doit être un tableau-
     * @return array|null[]|object[]
     */
    private function findOne(array $where = [], array $select = []) {
        $selectStr = count($select) > 0 ? implode(", ", $select) : "*";
        $queryStr = "SELECT $selectStr FROM $this->tableName";
        $params = [];
        if (count($where) > 0) {
            $queryStr .= " WHERE ";
            $last_key = array_key_last($where);
            foreach ($where as $field => $value) {
                $queryStr .= "$field = :$field";
                $params[":$field"] = $value;
                if ($last_key != $field) {
                    $queryStr .= " AND ";
                }
            }
        }

        $query = $this->pdo->prepare($queryStr);
        $query->setFetchMode(\PDO::FETCH_ASSOC);
        $query->execute($params);

        $res = $query->fetch();
        if ($res) {
            $class = new \ReflectionClass($this->class);
            return $class->newInstanceArgs([$res]);
        }
        return null;
    }

    /**
     * Fonction permettant de mettre à jours les valeurs d'un enregistrement
     * @param $obj Les nouvelles valeurs
     * @param int $id La clé primaire de l'enregistrement
     * @return void
     */
    public function update($obj, int $id) {
        $queryStr = "UPDATE $this->tableName SET ";
        $params = [];

        $class = new \ReflectionClass($obj);
        $this->handleLifecycle($obj, $class, fn($att) => $att->preUpdate);

        $properties = array_filter(
            $class->getProperties(),
            fn($item) => count($item->getAttributes()) == 0);
        $last = array_key_last($properties);
        foreach ($properties as $key => $property) {
            if (count($property->getAttributes()) > 0) { continue; }
            if ($property->getName() == "id") { continue; }
            $name = $property->getName();
            $value = $property->getValue($obj);

            $queryStr .= "$name = :$name";
            if ($last != $key) {
                $queryStr .= ", ";
            }

            $params[":$name"] = $value;
        }


        $queryStr .= " WHERE id = :id";

        $params[":id"] = $id;

        $query = $this->pdo->prepare($queryStr);
        $query->execute($params);
    }

    /**
     * Fonction permettant l'insertion d'un enregistrement
     * @param object $obj
     * @return void
     */
    public function insert($obj) {
        $queryStr = "INSERT INTO $this->tableName(";
        $valuesStr = " VALUES (";

        $class = new \ReflectionClass($obj);
        $this->handleLifecycle($obj, $class, fn($att) => $att->prePersist);

        $properties = array_filter(
            $class->getProperties(),
            fn($item) => count($item->getAttributes()) == 0);
        $last = array_key_last($properties);
        foreach ($properties as $key => $property) {
            if (count($property->getAttributes()) > 0) { continue; }
            if ($property->getName() == "id") { continue; }

            $name = $property->getName();
            $value = $property->getValue($obj);

            $queryStr .= "$name";
            $valuesStr .= ":$name";
            if ($last != $key) {
                $queryStr .= ", ";
                $valuesStr .= ", ";
            }

            $params[":$name"] = $value;
        }

        $queryStr .= ")";
        $valuesStr .= ");";

        $query = $this->pdo->prepare($queryStr. $valuesStr);
        $query->execute($params);
    }

    /**
     * Fonction permettant le lazy loading d'un ManyToOne
     */
    function loadManyToOne($id)
    {
        return $this->findOneById($id);
    }
    function loadManyToMany(string $joinTable, string $leftColumn, int $leftColumnValue, string $rightColumn): array
    {
        $queryStr = "SELECT t.* FROM $joinTable mm JOIN $this->tableName t ON t.id = mm.$rightColumn WHERE $leftColumn = :value";

        var_dump($queryStr);
        $query = $this->pdo->prepare($queryStr);
        $query->execute([":value" => $leftColumnValue]);


        $class = new \ReflectionClass($this->class);
        return array_map(fn($item) => $class->newInstanceArgs([$item]), $query->fetchAll());
    }
    function loadOneToOne($id)
    {
        return $this->findOneById($id);
    }

    /**
     * Fonction magique de PHP permettant l'appel de fonction non existante
     * @param string $method Le nom de la méthode
     * @param array $params Les paramètres de la méthode à appeler
     * @return array|null[]|object[]|null
     */
    public function __call(string $method, array $params) {
        //Récupération du paramètre SELECT
        $select = array_values(array_filter($params, fn($param) => is_array($param)));
        if (count($select) > 0) {
            $select = $select[0];
        }
        //Selection de la méthode de requêtes à executer
        if (str_starts_with($method, "findAll")) {
            //Récupération des chanmps de la clause WHERE
            $method = explode("By", $method);

            if (count($method) > 1) {
                $andConditions = explode("And", $method[1]);
                //Construction de la clause WHERE
                return $this->buildWhereParams($andConditions, $params, fn($where) => $this->findAll($where, $select));
            } else {
                return $this->findAll([], $select);
            }
        }
        elseif (str_starts_with($method, "findOne")) {
            $method = explode("By", $method);

            if (count($method) > 1) {
                $andConditions = explode("And", $method[1]);
                return $this->buildWhereParams($andConditions, $params, fn($where) => $this->findOne($where, $select));
            } else {
                return $this->findOne([], $select);
            }
        }

        return null;
    }

    /**
     * Fonction permettant la construction des paramètres WHERE de la requetes
     * @param array $fields
     * @param array $params
     * @param callable $callback
     * @return mixed
     */
    private function buildWhereParams(array $fields, array $params, callable $callback) {
        $where = [];
        for ($i = 0; $i < count($fields); $i++) {
            $field = strtolower($fields[$i]);
            $param = $params[$i];

            $where[$field] = $param;
        }
        return $callback($where);
    }

    /**
     * Fonction traitant le cycle de vie de l'entite (Avant l'insertion en DB, avant l'update, ...)
     * @param $obj
     * @param \ReflectionClass $class
     * @param callable $predicate
     * @return void
     */
    private function handleLifecycle($obj, \ReflectionClass $class, callable $predicate) {
        $methods = $class->getMethods();
        foreach ($methods as $method) {
            $attributes = $method->getAttributes(Lifecycle::class);
            foreach ($attributes as $attribute) {
                $att = $attribute->newInstance();
                if ($att instanceof Lifecycle) {
                    if ($predicate($att)) {
                        $method->invoke($obj);
                    }
                }
            }
        }
    }
}