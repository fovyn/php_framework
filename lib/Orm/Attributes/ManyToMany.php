<?php

namespace Framework\Orm\Attributes;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class ManyToMany extends Relation
{
    public string $joinTable;
    public string $leftColumn;
    public string $rightColumn;

    public function __construct(string $target, string $joinTable, string $leftColumn, string $rightColumn)
    {
        parent::__construct($target);
        $this->joinTable = $joinTable;
        $this->leftColumn = $leftColumn;
        $this->rightColumn = $rightColumn;
    }
}