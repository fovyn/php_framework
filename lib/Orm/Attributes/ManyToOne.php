<?php
namespace Framework\Orm\Attributes;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class ManyToOne extends Relation {
    public string $joinColum;

    public function __construct(string $target, string $joinColum)
    {
        parent::__construct($target);
        $this->joinColum = $joinColum;
    }
}