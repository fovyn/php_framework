<?php

namespace Framework\Orm\Attributes;

#[\Attribute(\Attribute::TARGET_METHOD)]
class Lifecycle
{
    public bool $prePersist;
    public bool $preUpdate;

    public function __construct(bool $prePersist = false, bool $preUpdate = false)
    {
        $this->prePersist = $prePersist;
        $this->preUpdate = $preUpdate;
    }
}