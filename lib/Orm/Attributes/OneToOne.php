<?php

namespace Framework\Orm\Attributes;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class OneToOne extends Relation
{
    public string $joinColumn;

    public function __construct(string $target, string $joinColumn)
    {
        parent::__construct($target);
        $this->joinColumn = $joinColumn;
    }


}