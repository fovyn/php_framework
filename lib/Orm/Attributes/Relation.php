<?php

namespace Framework\Orm\Attributes;
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Relation
{
    public string $target;

    public function __construct(string $target)
    {
        $this->target = $target;
    }
}
