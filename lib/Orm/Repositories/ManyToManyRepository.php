<?php

namespace Framework\Orm\Repositories;

interface ManyToManyRepository
{
    function loadManyToMany(string $joinTable, string $leftColumn, int $leftColumnValue, string $rightColumn): array;
}