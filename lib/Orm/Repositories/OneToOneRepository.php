<?php

namespace Framework\Orm\Repositories;

interface OneToOneRepository
{
    function loadOneToOne($id);
}