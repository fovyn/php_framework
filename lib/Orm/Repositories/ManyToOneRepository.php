<?php

namespace Framework\Orm\Repositories;

interface ManyToOneRepository
{
    function loadManyToOne($id);
}