<?php

namespace App\Models;

use Framework\Orm\Attributes\ManyToMany;
use Framework\Orm\Attributes\ManyToOne;
use Framework\Orm\Entity;

class Sujet extends Entity
{
    private int $id;
    private string $title;
    private string $description;
    private int $creatorId;

    // Plusieurs Sujet pour une User
    // Target => La classe cible de la relation
    // JoinColum => le nom du champs dans la table Sujet qui possèdent l'ID de l'utilisateur cible
    #[ManyToOne(target: User::class, joinColum: "creatorId")]
    private ?User $creator = null;

    // Plusieurs Sujet pour Plusieurs User
    // joinTable => Table de liaison
    // leftColumn => Le nom du champs dans la table de liaison qui possèdent l'id du sujet
    // rightColum => Le nom du champs dans la table de liaison qui possèdent l'id du user
    #[ManyToMany(target: User::class, joinTable: "sujet_user", leftColumn: "sujet_id", rightColumn: "user_id")]
    private ?array $subscribers = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Sujet
     */
    public function setId(int $id): Sujet
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Sujet
     */
    public function setTitle(string $title): Sujet
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Sujet
     */
    public function setDescription(string $description): Sujet
    {
        $this->description = $description;
        return $this;
    }

    public function getCreatorId() { return $this->creatorId; }
    public function setCreatorId($id) {
        $this->creatorId = $id;
        return $this;
    }

}