<!doctype html>
<html lang="fr">
<head>
    <?php require_once HEADER_TEMPLATE; ?>
</head>
<body>
<?php require_once NAVBAR_TEMPLATE; ?>

<div class="container">
    <form method="post">
        <div class="mb-3">
            <label for="title" class="form-label">Titre</label>
            <input type="text" class="form-control" id="title" name="title" value="<?= $sujet->getTitle() ?>">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea  class="form-control" id="title" name="description"><?= $sujet->getDescription() ?></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Editer</button>
    </form>
</div>
</body>
</html>