<?php

use Framework\Security\Security;

$canPerformAction = isset($user) && (Security::hasRole("ROLE_ADMIN"));
$isCreator = fn($subject) => isset($user) && $subject->getCreatorId() == $user->getId();

?>
<!doctype html>
<html lang="en">
<head>
    <?php require_once "./src/Views/templates/header.php" ?>
</head>
<body>
<?php require_once "./src/Views/templates/navbar.php"; ?>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($sujets as $sujet) { ?>
        <tr>
            <td><?= $sujet->getId() ?></td>
            <td><?= $sujet->getTitle() ?></td>
            <td><?= $sujet->getDescription() ?></td>
            <?php if ($canPerformAction || $isCreator($sujet)) { ?>
                <td>
                    <a class="btn btn-secondary" href="/sujet/update?id=<?= $sujet->getId() ?>">Edit</a>
                </td>
            <?php } ?>
        </tr>
    <?php } ?>
    </tbody>
</table>
</body>
</html>