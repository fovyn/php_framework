<!doctype html>
<html lang="fr">
<head>
    <?php require_once "./src/Views/templates/header.php" ?>
</head>
<body>
<?php require_once "./src/Views/templates/navbar.php" ?>
<h1>Security-Login</h1>
<form action="/login" method="post">
    <div class="form-field">
        <input type="text" name="username" id="username">
        <label for="username">Username</label>
    </div>
    <div class="form-field">
        <input type="password" name="password" id="password">
        <label for="password">Password</label>
    </div>
    <button type="submit">Se connecter</button>
</form>
</body>
</html>