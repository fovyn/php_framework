<!doctype html>
<html lang="fr">
<head>
    <?php require_once "./src/Views/templates/header.php" ?>
</head>
<body>
<?php require_once "./src/Views/templates/navbar.php" ?>
<h1>Security-register</h1>
<form action="/register" method="post">
    <div class="form-field">
        <input type="text" name="username" id="username">
        <label for="username">Username</label>
    </div>
    <div class="form-field">
        <input type="password" name="password" id="password">
        <label for="password">Password</label>
    </div>
    <button type="submit">S'enregistrer</button>
</form>
</body>
</html>