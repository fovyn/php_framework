<?php

namespace App\Repositories;

use App\Models\Sujet;
use Framework\Orm\Repository;

class SujetRepository extends Repository
{

    public function __construct()
    {
        parent::__construct(Sujet::class, "sujet");
    }
}