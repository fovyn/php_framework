<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends \Framework\Orm\Repository
{
    public function __construct()
    {
        parent::__construct(User::class, "security_user");
    }
}