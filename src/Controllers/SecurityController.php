<?php

namespace App\Controllers;

use App\Models\User;
use App\Repositories\UserRepository;
use Framework\Attributes\Controller;
use Framework\Attributes\Request;
use Framework\Http\Request as HttpRequest;
use Framework\Http\Response;

#[Controller]
class SecurityController
{

    #[Request(methods:[ "GET"], paths: ["/login"])]
    public function signInAction(HttpRequest $request) {
        return Response::send("security/signIn");
    }

    #[Request(methods: ["POST"], paths: ["/login"])]
    public function signInResultAction(HttpRequest $request, UserRepository $userRepository) {
        $username = $request->request("username");
        $password = $request->request("password");

        $user = $userRepository->findOneByUsernameAndPassword($username, $password);
        //$userRepository->findOne(["username" => $username, "password" => $password]);
        if ($user) {
            $request->session()->signIn((object)["user" => $user, "roles" => ["ROLE_USER"]]);
        }

        return Response::redirectToUrl("/");
    }

    #[Request(methods: ["GET"], paths: ["/logout"])]
    public function signOutAction(HttpRequest $request) {
        $request->session()->close();

        return Response::redirectToUrl("/");
    }

    #[Request(methods: ["GET"], paths: ["/register"])]
    public function register(HttpRequest $request) {
        return Response::send("security/register");
    }

    #[Request(methods: ["POST"], paths: ["/register"])]
    public function registerResultAction(HttpRequest $request, UserRepository $repo) {
        $username = $request->request("username");
        $password = $request->request("password");

        $user = new User();
        $user->setUsername($username)
            ->setPassword($password);

        $repo->insert($user);

        return Response::send("security/register");
    }
}