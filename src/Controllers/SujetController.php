<?php

namespace App\Controllers;

use App\Models\Sujet;
use App\Repositories\SujetRepository;
use App\Repositories\UserRepository;
use Framework\Attributes\Authorize;
use Framework\Attributes\Controller;
use Framework\Attributes\Request;
use Framework\Http\Response;
use Framework\Http\Request as HttpRequest;

#[Controller(prefix: "/sujet")]
class SujetController
{

    // GET /sujet, GET /sujet/, GET /sujet/list
    #[Request(methods: ["GET"], paths: ["", "/", "/list"])]
    public function listAction(SujetRepository $sujetRepository) {
        $sujets = $sujetRepository->findAll();
        return Response::send("sujet/list", ["sujets" => $sujets]);
    }


    //GET Formulaire non soumis
    //Post Formulaire soumis
    #[Authorize]
    #[Request(methods: ["GET", "POST"], paths: ["/new", "/create"])]
    public function createAction(HttpRequest $request, SujetRepository $sujetRepository) {
        $method = $request->method();
        $sujet = new Sujet(["title" => "", "description" => ""]);
        if ($method == "POST") {
            $title = $request->request("title");// $title = $_POST["title"];
            $description = $request->request("description");
            $creatorId = $request->session()->get("user")->getId();

            if (!empty($title) && !empty($description) && !empty($creatorId)) {
                $sujet = new Sujet([...$request->request(), "creatorId" => $creatorId]);
                $sujetRepository->insert($sujet);

                return Response::redirectToUrl("/sujet");
            }
        }
        return Response::send("sujet/create", ["sujet" => $sujet]);
    }

    #[Request(methods: ["GET", "POST"], paths: ["/update"])]
    public function updateAction(HttpRequest $request, SujetRepository $sujetRepository) {
        $method = $request->method();

        $id = intval($request->get("id"));
        $sujet = $sujetRepository->findOneById($id);

        if ($method == "POST") {
            $title = $request->request("title");// $title = $_POST["title"];
            $description = $request->request("description");
            $creatorId = $request->session()->get("user")->getId();

            if (!empty($title) && !empty($description) && !empty($creatorId)) {
                $sujet = new Sujet([...$request->request(), "creatorId" => $creatorId]);
                var_dump($sujet);
                $sujetRepository->update($sujet, $id);

                return Response::redirectToUrl("/sujet");
            }
        }

        return Response::send("sujet/update", ["sujet" => $sujet]);
    }
}