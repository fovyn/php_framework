<?php

namespace App\Controllers;

use App\Repositories\UserRepository;
use Framework\Attributes\Controller;
use Framework\Attributes\Request;
use Framework\Http\Request as HttpRequest;
use Framework\Http\Response;

#[Controller]
class HomeController
{

    // GET  , GET /, GET /index
    #[Request(methods: ["GET"], paths: ["", "/", "/index"])]
    public function indexAction() {
        return Response::send("home/index", ["username" => "Flavian"]);
    }

}