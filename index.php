<?php

require __DIR__. '/vendor/autoload.php';

define("HEADER_TEMPLATE", "./src/Views/templates/header.php");
define("NAVBAR_TEMPLATE", "./src/Views/templates/navbar.php");

$url = $_SERVER["REQUEST_URI"];
$requestMethod = $_SERVER["REQUEST_METHOD"];

$path = parse_url($url, PHP_URL_PATH);

// J'enregistre l'ensemble des controllers avec leurs urls
$diContainer = \Framework\DI\DiContainer::instance();
// Récupère un seul controller selon la méthode d'access (GET|POST) et l'url
$resolver = $diContainer->controller($requestMethod, $path);

//Par défaut => afficher la page d'erreur
$actionResult = \Framework\Http\Response::send("errors/404");
//Si j'ai un controller
if ($resolver) {
    //Si j'ai des paramètres de l'action
    $params = [];
    if (key_exists("params", $resolver)) {
        $params = $resolver["params"];
    }
    if (!$resolver["access"]) {
//        $actionResult = \Framework\Http\Response::send("errors/403");
        \Framework\Http\Response::redirectToUrl("/login");
    } else {
        //Récupération de l'action a effectuer
        $rMethod = new ReflectionMethod($resolver["ctrl"], $resolver["action"]);
        //Invoquation de l'action
        $actionResult = $rMethod->invokeArgs($resolver["ctrl"], $params);
    }
}
//Affiche la réponse à la requête
$actionResult->display();